use glib::Object;
use gtk::{
    self, prelude::*, Box, GridView, Image, Label, ListItem, Orientation, ScrolledWindow,
    SignalListItemFactory, Window,
};
use gtk::{glib, NoSelection};

mod my_object {
    mod imp {
        use gtk::glib;
        use gtk::subclass::prelude::*;

        #[derive(Default)]
        pub struct MyObject {}

        #[glib::object_subclass]
        impl ObjectSubclass for MyObject {
            const NAME: &'static str = "MyObject";
            type Type = super::MyObject;
        }

        impl ObjectImpl for MyObject {}
    }

    glib::wrapper! {
        pub struct MyObject(ObjectSubclass<imp::MyObject>);
    }

    impl MyObject {
        pub fn new() -> Self {
            glib::object::Object::new()
        }
    }
}

const APP_ID: &str = "net.tobico.GridTest";

fn main() -> glib::ExitCode {
    let app = gtk::Application::builder().application_id(APP_ID).build();
    app.connect_activate(build_ui);
    app.run()
}

fn build_ui(app: &gtk::Application) {
    let factory = SignalListItemFactory::new();
    factory.connect_setup(move |_, list_item| {
        let view: Image = Object::builder()
            .property("width-request", 80)
            .property("height-request", 80)
            .property("file", "image.png")
            .build();
        list_item
            .downcast_ref::<ListItem>()
            .expect("Needs to be ListItem")
            .set_child(Some(&view));
    });

    let store = gio::ListStore::new::<my_object::MyObject>();

    for _ in std::iter::repeat(()).take(5) {
        let object = my_object::MyObject::new();
        store.append(&object);
    }

    let model = NoSelection::new(Some(store));

    let grid_view: GridView = Object::builder()
        .property("factory", factory)
        .property("model", model)
        .build();

    let my_box: Box = Object::builder()
        .property("orientation", Orientation::Vertical)
        .build();

    my_box.append(&grid_view);

    let label: Label = Object::builder().property("label", "Bottom").build();

    my_box.append(&label);

    let scrolled_window: ScrolledWindow = Object::builder().property("child", my_box).build();

    let window: Window = Object::builder()
        .property("application", app)
        .property("default-width", 300)
        .property("default-height", 200)
        .property("child", scrolled_window)
        .build();

    window.present();
}
